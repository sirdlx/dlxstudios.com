
import { conf } from "./config";
import { initialData } from "./config";
import type { Config, Data } from "@/components/editor/types/Config";
import { Puck } from "@/components/editor/components/Puck";
import { Render } from "@/components/editor/components/Render";


export function Client({ isEdit }: { isEdit: boolean }) {


  // <Puck

  //   config={conf as unknown as Config<any, any, any>}
  //   data={{
  //     content: [],
  //     root: {
  //       props: {
  //         title: "string",
  //       },
  //     },
  //   }}
  //   onPublish={async (data: Data) => {
  //     localStorage.setItem("key", JSON.stringify(data));
  //   }}
  //   plugins={[]}
  //   headerPath={"path"}
  // />

  if (isEdit) {
    return (
      <div>
        <Puck

          onChange={(data) => {
            // console.log(data);
          }}
          config={conf}
          data={initialData['/']}

          // data={
          //   {
          //     content: [],
          //     root: {
          //       props: {
          //         title: "string",
          //       },
          //     }
          //   }
          // }

          onPublish={async (data: Data) => {
            localStorage.setItem('key', JSON.stringify(data));
          }}
          headerPath={'path'}

        />
      </div>
    );
  }

  if (initialData) {
    return <Render config={conf} data={initialData['/']} />;
  }

  return (
    <div
      style={{
        display: "flex",
        height: "100vh",
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <div>
        <h1>404</h1>
        <p>Page does not exist in session storage</p>
      </div>
    </div>
  );
}

export default Client;
