/* eslint-disable @next/next/no-img-element */
import React from "react";
import { ComponentConfig } from "@/editor/types/Config";
import styles from "./styles.module.css";
import { getClassNameFactory } from "@/editor/lib";


const getClassName = getClassNameFactory("Card", styles);


export type CardProps = {
  title: string;
  description: string;
  
  mode: "flat" | "card";
};

export const Card: ComponentConfig<CardProps> = {
  fields: {
    title: { type: "text" },
    description: { type: "textarea" },
   
    mode: {
      type: "radio",
      options: [
        { label: "card", value: "card" },
        { label: "flat", value: "flat" },
      ],
    },
  },
  defaultProps: {
    title: "Title",
    description: "Description",
   
    mode: "flat",
  },
  render: ({ title, description, mode }) => {
    return (
      <div className={getClassName({ [mode]: mode })}>
        <div className={getClassName("title")}>{title}</div>
        <div className={getClassName("description")}>{description}</div>
      </div>
    );
  },
};
