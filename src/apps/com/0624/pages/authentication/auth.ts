import type { APIRoute } from "astro";
import Pocketbase, { RecordAuthResponse } from "pocketbase";
import { userPb } from "src/kv_authstore";
export interface ErrorsSignin {
  email?: string;
  password?: string;
  confirm?: string;
  message?: string;
}
export const POST: APIRoute = async ({ request }) => {
  const errors: ErrorsSignin = { message: "", email: "", password: "" };
  let _email = "";
  let userData: RecordAuthResponse;

  console.log("Login::");

  const data = await request.formData();
  console.log("Login::data", data);

  const email = data.get("email");
  _email = email.toString();
  const password = data.get("password");

  if (typeof email !== "string") {
    //|| !isValidEmail(email)) {
    errors.email += "Email is not valid. ";
  }
  // else if (await isRegistered(email)) {
  //   errors.email += "Email is already registered. ";
  // }
  if (typeof password !== "string" || password.length < 6) {
    errors.password += "Password must be at least 6 characters.";
  }
  const hasErrors = Object.values(errors).some((msg) => msg);
  console.log("Login::hasErrors", hasErrors, errors);

  if (!hasErrors) {
    //   console.log("user", email, password);

    // try {
    userData = await userPb
      .collection("users")
      .authWithPassword(email.toString(), password.toString());
    console.log("Login::userData", userData);
    // return Response.json({ token: userData.token });
    // return Astro.redirect("/");
    // } catch (error) {
    //   if (error instanceof Error) {
    //     errors.message = error.message;
    //     console.error(error.message);
    //   }
    // }

    //   await registerUser({ name, email, password });
  }

  if (!email || !password) {
    return new Response(
      JSON.stringify({
        message: "Missing required fields",
      }),
      { status: 400 }
    );
  }
  // Do something with the data, then return a success response
  return new Response(
    JSON.stringify({
      message: "Success!",
    }),
    { status: 200 }
  );
};
