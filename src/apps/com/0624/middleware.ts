import { defineMiddleware } from "astro:middleware";
import { getAdminToken } from "src/kv";
import { userPb } from "src/kv_authstore";
// `context` and `next` are automatically typed

export const onRequest = defineMiddleware(
  async ({ request, redirect, locals, cookies }, next) => {
    // get admin token from pocketbase
    const aToken = await getAdminToken();
    locals.pocketbaseAdminToken = aToken;

    // console.log("defineMiddleware::aToken::", aToken);

    // // Check for domain routing first

    // const url = new URL(request.url);
    // const host = url.hostname;
    // console.log("defineMiddleware::URL::url.host::", url.hostname);
    // const isSub = host.split(".")[0];
    // console.log(`defineMiddleware::isSub:: ${host.split(".").length > 2}`);

    // const isProd = import.meta.env.PROD;
    // const isDev = import.meta.env.DEV;

    // if (isSub) {
    //   return redirect(
    //     request.url.replace(
    //       url.host,
    //       `${!isProd ? url.host.split(".")[1] : "dlxstudios.com"}/${
    //         url.host.split(".")[0] + url.pathname
    //       }`
    //     ),
    //     308
    //   );
    // }

    // Pocketbase User from cookies
    locals.pocketbaseUserClient = userPb;

    userPb.authStore.save(cookies.get("pb_auth").value);

    // console.log(request.headers.get("cookie"));

    // console.log(
    //   "defineMiddleware::cookie::",
    //   request.headers.get("cookie") || ""
    // );
    try {
      // get an up-to-date auth store state by verifying and refreshing the loaded auth model (if any)
      locals.pocketbaseUserClient.authStore.isValid &&
        (await locals.pocketbaseUserClient.collection("users").authRefresh());
    } catch (_) {
      // clear the auth store on failed refresh
      locals.pocketbaseUserClient.authStore.clear();
    }

    const response = await next();

    // send back the default 'pb_auth' cookie to the client with the latest store state
    response.headers.append(
      "set-cookie",
      locals.pocketbaseUserClient.authStore.exportToCookie()
    );

    return response;

    return Response.json({ magic: "fire" });
  }
);
