import PocketBase, { type AuthModel, type RecordModel } from "pocketbase";
import { ModelFromRecord, type StudioModel } from "./StudioModels";

export async function studioByID(
  id: string,
  pb: PocketBase
): Promise<StudioModel | any> {
  try {
    const studioWorkspace = await pb
      .collection("studio_studio")
      .getOne(id, {
        // fields: "",
        expand: "studio_apps,viewer_users,editor_users,owner",
      })
      .then((sw) => {
        // console.log("sw.cover", sw.cover);

        if (sw != null) {
          sw.cover = pb.getFileUrl(
            sw,
            sw.cover
            // { thumb: "320x0" }
          );
        }

        // console.log("sw.cover", sw.cover);

        return sw;
      })
      .then((sw) => ModelFromRecord<StudioModel>(sw))
      .then((sw) => {
        // sw.viewer_users.map((vu) => {
        //   vu.role = "Viewer";
        //   return vu;
        // });
        // sw.editor_users.map((vu) => {
        //   vu.role = "Editor";
        //   return vu;
        // });

        return sw;
      });

    return studioWorkspace;
  } catch (e) {
    return e as Error;
  }
}

export async function studiosByUserId(
  userId: string,
  pb: PocketBase
): Promise<StudioModel[] | any> {
  let studioUserWorkspaces: StudioModel[] = [];

  try {
    studioUserWorkspaces = await pb.collection("studio_studio").getFullList({
      expand: "studio_apps,viewer_users,editor_users,owner",
      sort: "-created",
    });
    // .collection("studio_workspace")
    // .getFullList({
    //   filter: `owner.id="${userId}" || editor_users.id="${userId}" || viewer_users.id="${userId}"`,
    //   sort: "-updated",
    //   expand: "studio_apps,viewer_users,editor_users,cover",
    // })
    // .then((wss) => {
    //   return wss.map((ws) => {
    //     // ws.id === "pqhd2domhd1c0gb"
    //     //   ? console.log(
    //     //       "s",
    //     //       ModelFromRecord<StudioModel>(ws).expand!
    //     //     )
    //     //   : null;

    //     const model = ModelFromRecord<StudioModel>(ws);

    //     model.viewer_users.map((vu) => {
    //       vu.role = "Viewer";
    //       return vu;
    //     });
    //     model.editor_users.map((eu) => {
    //       eu.role = "Editor";
    //       return eu;
    //     });

    //     // console.log("model.viewer_users", model.viewer_users);

    //     return model;
    //   });
    // });

    return studioUserWorkspaces;
  } catch (error) {
    return error as Error;
  }

  return studioUserWorkspaces as StudioModel[];
}
