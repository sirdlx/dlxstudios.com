import { type RecordModel } from "pocketbase";

export function ModelFromRecord<T = {}>(record: RecordModel) {
  let props = {};
  // console.log(record.expand);

  if (record.expand != undefined) {
    props = record.expand;
  }
  // console.log({ ...record, ...props } as T);
  return { ...record, ...props } as T;
}

export function ModelFromRecords<T = {}>(records: RecordModel[]): T[] {
  const arr: T[] = [];

  for (let index = 0; index < records.length; index++) {
    const record = records[index];
    let props = {};
    if (record.expand != undefined) {
      props = record.expand;
      arr.push({ ...record, ...props } as T);
    }
  }
  // console.log({ ...record, ...props } as T);
  return arr;
}

export interface StudioAppModel extends BaseModel {
  name: string;
  app_id?: string;
  collection: string;
  requires?: StudioAppModel[];
}

export interface StudioAccountModel extends BaseModel {
  name: string;
  email: string;
  user: string;
  studio_workspaces: StudioModel[];
  editor_of_studio_workspaces: StudioModel[];
  viewer_of_studio_workspaces: StudioModel[];
  role: "Viewer" | "Editor" | "Unassigned";
}

export enum RoleStudio {
  "Viewer",
  "Editor",
  "Unassigned",
}

export function GetRole(recordId: any) {}

export interface StudioModel extends BaseModel {
  title: string;
  owner: string;
  studio_apps: StudioAppModel[];
  cover: string;
  viewer_users: StudioAccountModel[];
  editor_users: StudioAccountModel[];
}

export interface BaseModel {
  id: string;
  collectionId: string;
  collectionName: string;
  created: string;
  updated: string;
  expand?: {
    [key: string]: any;
  };
}
