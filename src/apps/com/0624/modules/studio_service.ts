import PocketBase, { BaseAuthStore } from "pocketbase";

export async function loadFromCookie(
  pb: PocketBase,
  cookie: string
): Promise<string | null> {
  try {
    pb.authStore.loadFromCookie(cookie);
  } catch (error) {
    return null;
  }

  return null;
}

export const StudioPB = new PocketBase("https://api-pocketbase.fly.dev");
