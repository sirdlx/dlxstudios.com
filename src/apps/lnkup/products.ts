export interface Product {
  icon?: string;
  extra?: string[];
  icon_size?: number;
  id?: string;
  title: string;
  desc_short?: string;
  description?: string;
  price?: number;
  salePrice?: number;
  coverImage?: string;
  stripe_price?: string;
  stripe_product?: string;
  stripe_link?: string;
  variations?: Product[];
  features?: any[];
  quantities?: {
    amount: number;
    price: number;
    discount?: number;
    stripe_price?: string;
    stripe_product?: string;
  }[];
  quickInfo?: string[];
  includedImages?: string[];
  howItWorksImage?: string;
  howItWorks?: {
    title: string;
    description: string;
  }[];
  faq?: {
    question: string;
    answer: string;
  }[];
}

export const Products: Product[] = [
  {
    icon: "ic--round-qr-code",
    id: "1",
    title: "LnkUp QR Code",
    description:
      "The LnkUp QR Code is your premium link to use for all your print needs.",
    price: 49.99,
    stripe_price: "",
    stripe_product: "",
    // coverImage:
    //   "https://cdn.shopify.com/s/files/1/0542/1999/7359/files/dot-metal-basic.png?v=1701956769",

    quantities: [
      {
        amount: 1,
        price: 49.99,
        discount: 0,
      },
      {
        amount: 5,
        price: 89.99,
        discount: 10,
      },
      {
        amount: 10,
        price: 169.99,
        discount: 15,
      },
    ],
    quickInfo: [
      "Premium matte finish",
      "Wide scanning capability",
      "Unique QR code for easy connection",
    ],
    includedImages: [
      "https://example.com/lnkup-band-package.jpg",
      "https://example.com/lnkup-band-accessories.jpg",
    ],
    howItWorksImage: "https://example.com/lnkup-band-how-it-works.jpg",
    howItWorks: [
      {
        title: "Create your profile",
        description:
          "Set up your LnkUp profile with your contact information and social media links.",
      },
      {
        title: "Personalize your LnkUp Band",
        description:
          "Customize the look of your LnkUp Band with different colors and designs.",
      },
      {
        title: "Share your LnkUp",
        description:
          "Share your unique QR code or tap your LnkUp Band to instantly connect with others.",
      },
    ],
    faq: [
      {
        question: "How do I set up my LnkUp Band?",
        answer:
          "Setting up your LnkUp Band is easy! Simply download the LnkUp app, create your profile, and follow the on-screen instructions to connect your LnkUp Band.",
      },
      {
        question: "Can I customize the design of my LnkUp Band?",
        answer:
          "Yes, you can choose from a variety of colors and designs to personalize your LnkUp Band.",
      },
      {
        question: "How long does the battery last?",
        answer:
          "The LnkUp Band has a long-lasting battery that can last up to 6 months on a single charge.",
      },
    ],
  },
  {
    icon: "healthicons--patient-band-alt",
    id: "2",
    title: "LnkUp Band",
    desc_short:
      "The LnkUp Band has a premium matte stylish finish. Perfect for networking. ",
    description:
      "The LnkUp Band is easy to use and features a premium matte stylish finish that is both weighty and soft to the touch. It provides the widest scanning capability of all our LnkUp devices and comes equipped with a unique QR code on the back to ensure a foolproof connection with every smartphone on the market.",
    price: 124.99,
    stripe_product: "prod_QGQc2yNdzntl3P",

    // coverImage:
    //   "https://cdn.shopify.com/s/files/1/0542/1999/7359/files/dot-metal-basic.png?v=1701956769",
    variations: [
      {
        title: "Black",
        description: "Black in color",
        stripe_price: "price_1PPtlMJ33BywOndo6wFLmKnv",
        price: 124.99,
        stripe_product: "",
      },
      {
        title: "White",
        description: "White in color",
        stripe_price: "price_1PPtp2J33BywOndoyNw6DosI",
        price: 124.99,
        stripe_product: "",
      },
    ],
    quantities: [
      {
        amount: 1,
        price: 19.99,
        discount: 0,
      },
      {
        amount: 5,
        price: 89.99,
        discount: 10,
      },
      {
        amount: 10,
        price: 169.99,
        discount: 15,
      },
    ],
    quickInfo: [
      "Premium matte finish",
      "Wide scanning capability",
      "Unique QR code for easy connection",
    ],
    includedImages: [
      "https://example.com/lnkup-band-package.jpg",
      "https://example.com/lnkup-band-accessories.jpg",
    ],
    howItWorksImage: "https://example.com/lnkup-band-how-it-works.jpg",
    howItWorks: [
      {
        title: "Create your profile",
        description:
          "Set up your LnkUp profile with your contact information and social media links.",
      },
      {
        title: "Personalize your LnkUp Band",
        description:
          "Customize the look of your LnkUp Band with different colors and designs.",
      },
      {
        title: "Share your LnkUp",
        description:
          "Share your unique QR code or tap your LnkUp Band to instantly connect with others.",
      },
    ],
    faq: [
      {
        question: "How do I set up my LnkUp Band?",
        answer:
          "Setting up your LnkUp Band is easy! Simply download the LnkUp app, create your profile, and follow the on-screen instructions to connect your LnkUp Band.",
      },
      {
        question: "Can I customize the design of my LnkUp Band?",
        answer:
          "Yes, you can choose from a variety of colors and designs to personalize your LnkUp Band.",
      },
      {
        question: "How long does the battery last?",
        answer:
          "The LnkUp Band has a long-lasting battery that can last up to 6 months on a single charge.",
      },
    ],
  },
  {
    icon: "solar--sticker-circle-bold",
    id: "3",
    title: "LnkUp Sticker",
    desc_short: "The LnkUp Sticker is designed for durability and easy to use.",
    description:
      "The LnkUp Sticker is designed for durability and easy to use. It features a dome-shaped protective outer layer of clear epoxy to keep it safe from harm. You can get creative and place it on any surface imaginable!",
    price: 74.99,
    stripe_price: "",
    stripe_product: "",
    // coverImage:
    //   "https://cdn.shopify.com/s/files/1/0542/1999/7359/files/dot-metal-basic.png?v=1701956769",
    variations: [
      {
        title: "Black",
        description: "Black in color",
        stripe_price: "",
        stripe_product: "",
      },
      {
        title: "White",
        description: "White in color",
        stripe_price: "",
        stripe_product: "",
      },
    ],
    quantities: [
      {
        amount: 1,
        price: 24.99,
        discount: 0,
      },
      {
        amount: 10,
        price: 199.99,
        discount: 20,
      },
      {
        amount: 25,
        price: 449.99,
        discount: 25,
      },
    ],
    quickInfo: [
      "Durable dome-shaped design",
      "Clear epoxy protective layer",
      "Versatile placement options",
    ],
    includedImages: [
      "https://example.com/lnkup-sticker-package.jpg",
      "https://example.com/lnkup-sticker-surfaces.jpg",
    ],
    howItWorksImage: "https://example.com/lnkup-sticker-how-it-works.jpg",
    howItWorks: [
      {
        title: "Create your profile",
        description:
          "Set up your LnkUp profile with your contact information and social media links.",
      },
      {
        title: "Personalize your LnkUp Sticker",
        description: "Choose the color and design of your LnkUp Sticker.",
      },
      {
        title: "Place your LnkUp Sticker",
        description:
          "Stick your LnkUp Sticker on any surface, and share your unique QR code to connect with others.",
      },
    ],
    faq: [
      {
        question: "How do I set up my LnkUp Sticker?",
        answer:
          "Setting up your LnkUp Sticker is easy! Simply download the LnkUp app, create your profile, and follow the on-screen instructions to connect your LnkUp Sticker.",
      },
      {
        question: "Can I remove and reuse the LnkUp Sticker?",
        answer:
          "Yes, the LnkUp Sticker can be removed and reused on different surfaces without losing its adhesive properties.",
      },
      {
        question: "Is the LnkUp Sticker waterproof?",
        answer:
          "Yes, the LnkUp Sticker is waterproof and can withstand exposure to moisture and outdoor conditions.",
      },
    ],
  },
  {
    icon: "clarity--bundle-line",
    id: "4",
    title: "LnkUp Bundle",
    desc_short:
      "This bundle includes the LnkUp QR Code, LnkUp Band and LnkUp Sticker.",
    description:
      "This bundle includes the LnkUp QR Code, LnkUp Band and LnkUp Sticker, giving you the ultimate versatility in how you share your digital business card. Wear the band for easy access or stick the sticker on any surface for maximum visibility.",
    price: 199.99,
    stripe_price: "",
    stripe_product: "",

    // coverImage:
    //   "https://cdn.shopify.com/s/files/1/0542/1999/7359/files/dot-metal-basic.png?v=1701956769",

    variations: [
      {
        title: "Black (Both)",
        price: 199.99,
        description: "Black in color",
        stripe_price: "",
        stripe_product: "",
      },
      {
        title: "White (Both)",
        price: 199.99,
        description: "White in color",
        stripe_price: "",
        stripe_product: "",
      },

      {
        title: "Black Band/White Sticker",
        price: 199.99,
        description: "Black in color",
        stripe_price: "",
        stripe_product: "",
      },
      {
        title: "White Band/Black Sticker",
        price: 199.99,
        description: "White in color",
        stripe_price: "",
        stripe_product: "",
      },
    ],
    quantities: [
      {
        amount: 1,
        price: 199.99,
        discount: 0,
      },
      {
        amount: 10,
        price: 1499.99,
        discount: 15,
      },
      {
        amount: 50,
        price: 1999.99,
        discount: 20,
      },
    ],
    quickInfo: [
      "Includes LnkUp Band, LnkUp Sticker, and QR Code",
      "Premium matte finish and clear epoxy layer",
      "Wide scanning capability and unique QR codes",
    ],
    includedImages: [
      "https://example.com/lnkup-bundle-package.jpg",
      "https://example.com/lnkup-bundle-accessories.jpg",
    ],
    howItWorksImage: "https://example.com/lnkup-bundle-how-it-works.jpg",
    howItWorks: [
      {
        title: "Create your profile",
        description:
          "Set up your LnkUp profile with your contact information and social media links.",
      },
      {
        title: "Customize your LnkUp devices",
        description:
          "Choose colors and designs for your LnkUp Band and LnkUp Sticker.",
      },
      {
        title: "Share your LnkUp",
        description:
          "Share your unique QR codes or tap your LnkUp devices to instantly connect with others.",
      },
    ],
    faq: [
      {
        question: "How do I set up the LnkUp Bundle?",
        answer:
          "Setting up the LnkUp Bundle is easy! Simply download the LnkUp app, create your profile, and follow the on-screen instructions to connect your LnkUp Band and LnkUp Sticker.",
      },
      {
        question: "Can I use the LnkUp Band and LnkUp Sticker separately?",
        answer:
          "Yes, the LnkUp Bundle includes both devices, and you can use them separately or together as needed.",
      },
      {
        question: "Is the LnkUp Bundle waterproof?",
        answer:
          "Yes, both the LnkUp Band and LnkUp Sticker in the bundle are waterproof and can withstand exposure to moisture and outdoor conditions.",
      },
    ],
  },
];
