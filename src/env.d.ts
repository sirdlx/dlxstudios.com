/// <reference path="../.astro/types.d.ts" />
/// <reference types="astro/client" />
declare namespace App {
  interface Locals {
    pocketbaseUserClient?: Pocketbase;
    pocketbaseAdminToken?: string;
  }
}

// https://docs.astro.build/en/guides/environment-variables/#intellisense-for-typescript
interface ImportMetaEnv {
  readonly SITE: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}

interface Task {
  id: string;
  name: string;
}

interface Post {
  id: string;
  title: string;
  active: boolean;
}

interface TypedPocketBase extends PocketBase {
  collection(idOrName: string): RecordService; // default fallback for any other collection
  collection(idOrName: "tasks"): RecordService<Task>;
  collection(idOrName: "posts"): RecordService<Post>;
}
