import type { ComponentConfig, DefaultRootProps, RootDataWithProps } from "@measured/puck";
import { ReactNode } from "react";

type Category<ComponentName> = {
  components?: ComponentName[];
  title?: string;
  visible?: boolean;
  defaultExpanded?: boolean;
};

export type Config<
  Props extends Record<string, any> = Record<string, any>,
  RootProps extends DefaultRootProps = DefaultRootProps,
  CategoryName extends string = string,
> = {
  categories?: Record<CategoryName, Category<keyof Props>> & {
    other?: Category<keyof Props>;
  };
  components: {
    [ComponentName in keyof Props]: Omit<
      ComponentConfig<Props[ComponentName], Props[ComponentName]>,
      "type"
    >;
  };
  root?: Partial<
    ComponentConfig<
      RootProps & {
        children?: ReactNode;
      },
      Partial<
        RootProps & {
          children?: ReactNode;
        }
      >,
      RootDataWithProps
    >
  >;
};

export const config: Config = {
    components: {
      'HeadingBlock': {
        fields: {
          title: { type: "text" },
        },
        defaultProps: {
          title: "Heading",
        },
        render: ({ title }) => (
          <div >
            <h1>{title}</h1>
          </div>
        ),
      },
    },
  };