export { default as StudioPage } from "../apps/com/0624/pages/studio/[id]/index.astro";
export { default as SigninPage } from "../apps/com/0624/pages/authentication/sign-in.astro";
export { default as SignupPage } from "../apps/com/0624/pages/authentication/sign-up.astro";
