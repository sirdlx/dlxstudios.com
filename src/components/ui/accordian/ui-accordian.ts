import { LitElement, css, unsafeCSS, html } from "lit";
import { customElement, property, query, queryAll } from "lit/decorators.js";

@customElement("ui-accordion")
export class UiAccordion extends LitElement {
  @property({ type: Boolean }) open = false;

  @query("ui-accordion-item")
  private accordionItem?: UiAccordionItem;

  @queryAll("ui-accordion-item")
  private accordionItems?: NodeListOf<UiAccordionItem>;

  @property({ attribute: "styles", reflect: true, type: Object }) styles =
    false;

  static styless = css`
    :host {
      display: block;
      color: red;
    }
    .accordion-item {
      border-bottom: 100px solid var(--border-color, #e5e7eb);
    }

    .accordion-trigger {
      display: flex;
      flex: 1;
      align-items: center;
      justify-content: space-between;
      padding: 1rem 0;
      font-weight: 500;
      transition: all 0.2s ease;
      cursor: pointer;
    }

    .accordion-trigger:hover {
      text-decoration: underline;
    }

    .accordion-trigger[open] .chevron {
      transform: rotate(180deg);
    }

    .accordion-content {
      overflow: hidden;
      transition: all 0.2s ease;
    }

    .accordion-content[open] {
      animation: accordion-down 0.2s ease-out;
    }

    .accordion-content:not([open]) {
      animation: accordion-up 0.2s ease-out;
    }

    @keyframes accordion-down {
      from {
        opacity: 0;
        max-height: 0;
      }
      to {
        opacity: 1;
        max-height: 100%;
      }
    }

    @keyframes accordion-up {
      from {
        opacity: 1;
        max-height: 100%;
      }
      to {
        opacity: 0;
        max-height: 0;
      }
    }
  `;

  render() {
    return html`
      <div class="accordion" style=${unsafeCSS(this.styles)}>
        accordion
        <slot></slot>
      </div>
    `;
  }
}

@customElement("ui-accordion-item")
export class UiAccordionItem extends LitElement {
  @property({ type: Boolean }) open = false;

  @query("ui-accordion-trigger")
  private accordionTrigger?: UiAccordionTrigger;

  @query("ui-accordion-content")
  private accordionContent?: UiAccordionContent;

  connectedCallback() {
    super.connectedCallback();
    this.accordionTrigger?.addEventListener("click", this.toggleOpen);
    console.log(this);
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    this.accordionTrigger?.removeEventListener("click", this.toggleOpen);
  }

  private toggleOpen = () => {
    this.open = !this.open;
    this.accordionContent?.setAttribute("open", `${this.open}`);
    console.log(this.open);
  };

  render() {
    return html`
      <style>
        ${this.open &&
        `
          :host {
            border-bottom: 10px solid var(--border-color, red);
          }
        `}
      </style>
      <div class="accordion-item">
        ${this.open}
        <slot></slot>
      </div>
    `;
  }
}

@customElement("ui-accordion-trigger")
export class UiAccordionTrigger extends LitElement {
  render() {
    return html`
      <button class="accordion-trigger">
        <slot></slot>
        <span class="chevron"> >< </span>
      </button>
    `;
  }
}

@customElement("ui-accordion-content")
export class UiAccordionContent extends LitElement {
  @property({ type: Boolean }) open = false;

  render() {
    return html`
      <div class="accordion-content" ?open=${this.open}>
        <slot></slot>
      </div>
    `;
  }
}
