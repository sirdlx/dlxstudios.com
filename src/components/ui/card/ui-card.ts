import { html, LitElement, css } from "lit";
import {
  customElement,
  property,
  eventOptions,
  query,
} from "lit/decorators.js";
export class UICard extends LitElement {
  @property({ type: String }) borderRadius: "1rem" | any = "1rem";

  render() {
    return html`
      <style>
        :host {
          display: flex;
          border-radius: 1rem;
          background-color: green;
        }
      </style>
      <div class="surface">
        <slot />
      </div>
    `;
  }
}

customElements.define("ui-card", UICard);

export class UICardBody extends LitElement {
  @property({ type: String }) padding: string | null = "1rem";

  render() {
    return html`
      <style>
        :host {
          padding: 1rem;
          padding: ${this.padding};s
          // background-color: green;
          display: block;
        }
      </style>
      <slot></slot>
    `;
  }
}

customElements.define("ui-card-body", UICardBody);

export class UICardHeader extends LitElement {
  @property({ type: String }) padding: string | null = "1rem";

  render() {
    return html`
      <style>
        :host {
          padding: ${this.padding};
        }
      </style>
      <slot></slot>
    `;
  }
}

customElements.define("ui-card-header", UICardHeader);
