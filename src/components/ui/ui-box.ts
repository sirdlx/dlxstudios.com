import { html, LitElement, css } from "lit";
import {
  customElement,
  property,
  eventOptions,
  query,
} from "lit/decorators.js";
export class UIBox extends LitElement {
  @property({ type: String }) flexDirection: string | null = null;
  @property({ type: String }) flexWrap: string | null = null;
  @property({ type: String }) justifyContent: string | null = null;
  @property({ type: String }) alignItems: string | null = null;
  @property({ type: String }) alignContent: string | null = null;
  @property({ type: Number }) flexGrow: number | null = null;
  @property({ type: Number }) flexShrink: number | null = null;
  @property({ type: String }) flexBasis: string | null = null;
  @property({ type: String, reflect: true }) height: string | null = null;
  @property({ type: String }) width: string | null = null;
  @property({ type: String }) backgroundColor: string | null = null;
  @property({ type: String }) textColor: string | null = null;
  @property({ type: String }) gridTemplateColumns: string | null = null;
  @property({ type: String }) gridTemplateRows: string | null = null;
  @property({ type: String }) gridGap: string | null = null;
  @property({ type: String }) gridAutoFlow: string | null = null;
  @property({ type: String }) gridAutoColumns: string | null = null;
  @property({ type: String }) gridAutoRows: string | null = null;
  @property({ type: String }) gridTemplateAreas: string | null = null;
  @property({ type: String }) display: "flex" | "grid" | "block" = "block";

  render() {
    return html`
      <style>
        :host {
          display: ${this.display === "block" ? "block" : this.display};
          display: flex;
          flex-direction: ${this.flexDirection};
          flex-wrap: ${this.flexWrap};
          justify-content: ${this.justifyContent};
          align-items: ${this.alignItems};
          align-content: ${this.alignContent};
          flex-grow: ${this.flexGrow};
          flex-shrink: ${this.flexShrink};
          flex-basis: ${this.flexBasis};
          height: ${this.height};
          width: ${this.width};
          background-color: ${this.backgroundColor};
          color: ${this.textColor};
          grid-template-columns: ${this.gridTemplateColumns};
          grid-template-rows: ${this.gridTemplateRows};
          grid-gap: ${this.gridGap};
          grid-auto-flow: ${this.gridAutoFlow};
          grid-auto-columns: ${this.gridAutoColumns};
          grid-auto-rows: ${this.gridAutoRows};
          grid-template-areas: ${this.gridTemplateAreas};
        }
      </style>
      <slot></slot>
    `;
  }
}

customElements.define("ui-box", UIBox);
