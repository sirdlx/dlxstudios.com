import { createClient, type VercelKV } from "@vercel/kv";
import PocketBase from "pocketbase";

export const kv = createClient({
  url: import.meta.env.KV_REST_API_URL,
  token: import.meta.env.KV_REST_API_TOKEN,
});

export async function getAdminToken(): Promise<string> {
  let adminToken: string;
  try {
    adminToken = await kv.get("adminToken");
    console.log(`kv::adminToken::${adminToken}`);
    if (adminToken == null) {
      return await setAdminToken(kv);
    }
  } catch (error) {
    // Handle errors
    console.error(error);
    // return await setAdminToken(kv);
  }
  return adminToken;
}

export async function setAdminToken(kv: VercelKV): Promise<string> {
  try {
    const pb = new PocketBase("https://api-pocketbase.fly.dev");
    const adminData = await pb.admins.authWithPassword(
      "cloud@dlxstudios.com",
      "dlxCl0ud1030@"
    );
    await kv.set("adminToken", adminData.token, { ex: 100, nx: true });
    console.log(`kv::setAdminToken::${adminData.token}`);
    return adminData.token;
  } catch (error) {
    // Handle errors
    console.error(error);
  }
}

export async function getUserToken(): Promise<string> {
  let userToken: string;
  try {
    userToken = await kv.get("adminToken");
    // console.log(`adminToken::${adminToken}`);
    if (userToken == null) {
      return await setAdminToken(kv);
    }
  } catch (error) {
    // Handle errors
    console.error(error);
    // return await setAdminToken(kv);
  }
  return userToken;
}

export async function setUserToken(
  kv: VercelKV,
  userID: string,
  token: string
): Promise<string> {
  try {
    await kv.set(`userToken:${userID}`, token, { ex: 100, nx: true });
    return token;
  } catch (error) {
    // Handle errors
    console.error(error);
  }
}
