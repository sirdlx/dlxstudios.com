import PocketBase, { BaseAuthStore } from "pocketbase";
import { kv, setUserToken } from "./kv";

class CustomAuthStore extends BaseAuthStore {
  save(token, model) {
    console.log(token);
    console.log("model.record", model);

    if (!token) return;

    try {
      setUserToken(kv, model.record.id, token).then((v) => {
        console.log(`setUserToken ::`, v);
      });
      return token;
    } catch (error) {
      // Handle errors
      console.error(error);
    }
    super.save(token, model);
  }
}

export const userPb: PocketBase = new PocketBase(
  "https://api-pocketbase.fly.dev"
  // new CustomAuthStore()
);
