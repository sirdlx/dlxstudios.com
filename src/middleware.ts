import { defineMiddleware } from "astro:middleware";
// `context` and `next` are automatically typed

import { getAdminToken } from "./kv";
import { userPb } from "./kv_authstore";
export const onRequest = defineMiddleware(
  async ({ request, redirect, locals }, next) => {
    const aToken = await getAdminToken();
    locals.pocketbaseAdminToken = aToken;

    locals.pocketbaseUserClient = userPb;

    locals.pocketbaseUserClient.authStore.loadFromCookie(
      request.headers.get("cookie") || ""
    );

    try {
      // get an up-to-date auth store state by verifying and refreshing the loaded auth model (if any)
      locals.pocketbaseUserClient.authStore.isValid &&
        (await locals.pocketbaseUserClient.collection("users").authRefresh());
    } catch (_) {
      // clear the auth store on failed refresh
      locals.pocketbaseUserClient.authStore.clear();
    }

    // const url = new URL(request.url);
    // const isSub =
    //   !url.host.startsWith("dlxstudios.com") || !url.host.startsWith("localhost");
    // // console.log("URL::isSub::", url.host.split(".")[0]);
    // // console.log(url.host.split(".")[1]);
    // console.log(`url.pathname ${url.host.split(".")[0] + url.pathname}`);

    // const isProd = import.meta.env.PROD;
    // const isDev = import.meta.env.DEV;

    // if (isSub) {
    //   return redirect(
    //     request.url.replace(
    //       url.host,
    //       `${!isProd ? url.host.split(".")[1] : "dlxstudios.com"}/${
    //         url.host.split(".")[0] + url.pathname
    //       }`
    //     ),
    //     308
    //   );
    // }

    // return next();

    const response = await next();

    // send back the default 'pb_auth' cookie to the client with the latest store state
    response.headers.append(
      "set-cookie",
      locals.pocketbaseUserClient.authStore.exportToCookie()
    );

    return response;
  }
);
