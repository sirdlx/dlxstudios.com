import preact from "@astrojs/preact";
import { defineConfig } from "astro/config";
import vercel from "@astrojs/vercel/serverless";
import lit from "@astrojs/lit";
import tailwind from "@astrojs/tailwind";
import sitemap from "@astrojs/sitemap";

import icon from "astro-icon";

// https://astro.build/config
export default defineConfig({
  srcDir: "./src/apps/com/0624",
  output: "server",
  // site: "https://dlxstudios.com",

  server: {
    host: "0.0.0.0",
    port: 4322
  },
  adapter: vercel({
    webAnalytics: {
      enabled: true
    }
  }),
  integrations: [, lit(), preact({
    compat: true
  }), tailwind(), sitemap(), icon()]
});